import React, { Component } from "react";
import Cards from "./components/cards/cards/cards";
import Cart from "./components/cart/Cart";
import "./App.css"
export default class App extends Component {
  render() {
    return (
      <div className="App-div">
        
        <Cards/>
        
        <Cart />
        
      </div>
    );
  }
}
