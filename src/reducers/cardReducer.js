
import data from "../data.json";


const DATA_CHANGE ="DATA_CHANGE";
const CART_ACTION ="CART_ACTION";
const CART_STATUS="CART_STATUS";
const ADD="ADD";
const SUBTRACT="SUBSRACT";
const CHECK_OUT ="CHECK_OUT";

//cards
//productcards
//cart
//checkout

let initialState={
    products:data.products,
    activeStates:{
        XS:false,
        S:false,
        M:false,
        ML:false,
        L:false,
        XL:false,
        XXL:false
    },
    array:{},
    cartData:[],
    count:0,
    amount:0,
    cartStatus:false
   
}


let states={
    XS:false,
        S:false,
        M:false,
        ML:false,
        L:false,
        XL:false,
        XXL:false,
        ALL:true
}

//For Event happened on clicking the Size buttons
export let changeValue=(event)=>{
    states[event.target.value]=!states[event.target.value];
    let finalData=[];
    if(states[event.target.value]){
        event.target.className="black-button"
    }else{
        event.target.className="white-button"
    }
    Object.keys(states).map((key)=>{
        if(states[key]){
            data.products.forEach((el)=>{
                if(el.availableSizes.includes(key)){
                    finalData.push(el)
                }
            })
        }
        
    })
    
    
    
    return{
        type:DATA_CHANGE,
        data:[...new Set(finalData)],
        ActiveState:states
    }
}


//event for Adding the items to the cart

let finalData={}
export let cartAction=(e)=>{

        let dataGet=()=>{
            let array;
            data.products.map((el)=>{
                           
                if(e.target.id==el.id){
                    
                    array= el
                }
            })
            return array

        }

      if(finalData[e.target.id]){
        finalData[e.target.id]={
            
            ...finalData[e.target.id],
            "count":finalData[e.target.id].count+1
        }
      }else{
        
        finalData[e.target.id]={
            "data":dataGet(),
            "count":1
        }
      
    }
    
    
    
return {
    type:CART_ACTION,
    data:finalData,
    count:0
    
}

    
}

// 


export let cartEvent=()=>{
    return{
        type:CART_STATUS
    }
}

// for adding items inside the cart so we use the same add to card function 

export let addItems=(e)=>{

    return {
        type:ADD
    }
}

// for subtrating items inside the cart so we use change the subtracting funtion 

export let subtractItems=(e)=>{
    let dataGet=()=>{
            let array;
            data.products.map((el)=>{
                           
                if(e.target.id==el.id){
                    
                    array= el
                }
            })
            return array

        }

      if(finalData[e.target.id]){
        finalData[e.target.id]={
            
            ...finalData[e.target.id],
            "count":finalData[e.target.id].count-1
        }
      }else{
        
        finalData[e.target.id]={
            "data":dataGet(),
            "count":1
        }
      
    }
    
    
    
return {
    type:CART_ACTION,
    data:finalData,
    
    
}
}

//event for checkout

export let checkOut=(amount,keys)=>{
    alert(`Your Total cart value is $${amount}`)
    return{
        type:CHECK_OUT,
        
    }
}




export const dataReducer=(state=initialState,action)=>{
    switch(action.type){
       case DATA_CHANGE:
        return {
            ...state,
            products:action.data,
            activeStates:action.ActiveState
        }
        case CART_ACTION:
            console.log(Object.values(action.data)[0],"hi");
            return {
                ...state,
                array:action.data,
                count:state.count+1,
                amount:(Number(state.amount)+Number(Object.values(action.data)[0].data.price) ).toFixed(2)
            }
        case CART_STATUS:
            return {
                ...state,
                cartStatus:!state.cartStatus
            }

        case ADD:
            console.log("hi");
            return {
                ...state,
                count:state.count+1
            }

        case SUBTRACT:
            return {
                ...state,
                count:state.count<1?0:state.count-1
            }
        case CHECK_OUT:

            return{
                
                    ...state,
                action:Object.keys(state.array).forEach(keys=>{
                    delete state.array[keys]
                })
                         
            }

            
    default:return state
    }
    
}
