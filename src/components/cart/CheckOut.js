import React, { Component } from "react";
import {connect} from "react-redux"
import "./Cart.css"
import { cartAction,subtractItems } from "../../reducers/cardReducer";

class CheckOut extends Component {
  
  render(){
    
    
    let products = require(`./../../static/products/${this.props.data.sku}_1.jpg`);
    if(this.props.count>0){
        return (
            <div className="cart-object-div">
              <img src={products} alt={this.props.data.title} className="cart-images" />
              <div className="dress-details">
                <h2 className="dress-heading">{this.props.data.title}</h2>
                <div className="size-and-style">
                  <p >{this.props.data.availableSizes[0]} |</p>
                  <p>{this.props.data.style?this.props.data.style:this.props.data.title}</p>
                </div>
                <p className="quantity">{`Quantity: ${this.props.count}`}</p>
              </div>
              <div>
                <h2 className="price">{`$ ${this.props.data.price}`}</h2>
                <div>
                  <button onClick={(e)=>this.props.subtractItems(e) } id={this.props.id}>-</button>
                  <button onClick={(e)=>this.props.cartAction(e)} id={this.props.id}>+</button>
                </div>
              </div>
            </div>
          );
    }
        
    
    
  } 
}

const mapStateToProps=state=>{
    return {
        
        array:state.dataReducer.array
    }
}
const mapDispatchToProps = () => {
    return {
        cartAction,
      subtractItems,
    
    };
  };

export default connect(mapStateToProps,mapDispatchToProps())(CheckOut);
