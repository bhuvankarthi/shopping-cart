import React, { Component } from "react";
import { connect } from "react-redux";
import "../../App.css";
import CheckOut from "./CheckOut";
import { FaShoppingCart } from "react-icons/fa";
import { GiCrossedBones } from "react-icons/gi";
import { cartEvent ,checkOut} from "../../reducers/cardReducer";
class Cart extends Component {
  render() {
    
    let array = [];
    let keys=Object.keys(this.props.array)
    Object.keys(this.props.array).map((key) => {
      array.push(this.props.array[key]);
    });
    let count = 0
    Object.keys(array).map((key)=>{
        count+=array[key].count
    })
    let amount=0;
    array.map((el)=>{
        amount+=Number(el.data.price)*Number(el.count)
    })

    if (this.props.cartStatus === true) {
      // console.log(this.props.itemsOfCart);
      return (
        <div className="total-cart">
          <GiCrossedBones className="cart-false" onClick={()=>this.props.cartEvent()}/>
          <FaShoppingCart className="bag-icon-cart" />
          <p className="checkout-cart-number">{count}</p>
          <p className="products-word">Products:</p>
          <div className="cart-dress">
          
            {array.map((el, index) => {
              // console.log(el);
              return <CheckOut id={el.data.id} data={el.data} count={el.count} />;
            })}
          </div>
          <footer className="total-footer">
            <div className="subtotal-footer">
              <h2>SUBTOTAL</h2>
              <h1 className="total-amount">$ {amount.toFixed(2)}</h1>
            </div>
            <button className="checkout-button" onClick={()=>this.props.checkOut(amount,keys)}>CHECKOUT</button>
          </footer>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    total: state.dataReducer,
    array: state.dataReducer.array,
    itemsOfCart: state.dataReducer.itemsOfCart,
    amount: state.dataReducer.amount,
    cartStatus: state.dataReducer.cartStatus,
    count:state.dataReducer.count
  };
};

const mapDispatchToProps = () => {
  return {
    
    cartEvent,
    checkOut
  };
};
export default connect(mapStateToProps,mapDispatchToProps())(Cart);
