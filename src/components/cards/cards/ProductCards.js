import React, { Component } from 'react';
import {connect} from "react-redux";
import "./cards.css"
import data from "../../../data.json"
import { cartAction } from '../../../reducers/cardReducer';

class ProductCards extends Component {
  
  
  render() {
    let siteData=[]
    let count=data.products.length;
    console.log(this.props.state.length);
    if(this.props.state.length>0){
      siteData=this.props.state
      count=this.props.state.length
    }else{
      siteData=data.products
      count=data.products.length
    }
    
    return(
      
        siteData.map(item=>{
            let products = require( `../../../static/products/${item.sku}_1.jpg`)
            return(
                <div className='separate-product'>
                
                 <img src={products} alt={`${item.title}`} className="product-images"/>
                 <p>{item.title}</p>
                 <hr className='hr-line'/>
                 <br/>
                 <h3>{`${item.currencyFormat} ${item.price}`}</h3>
                 <button className='add-to-cart' onClick={(e)=>this.props.cartAction(e)} id={item.id}>Add to cart</button>
                </div>
            )
        })
    )
    
  }
}

const mapStateToProps = (state) => {
    return {
      state: state.dataReducer.products,
      array:state.dataReducer.array,
      count:state.dataReducer.count
      
    };
  };
  const mapDispatchToProps=()=>{
    return{
      cartAction
    }
  }

  export default connect(mapStateToProps,mapDispatchToProps())(ProductCards);
