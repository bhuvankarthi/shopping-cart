import React, { Component } from "react";
import { connect } from "react-redux";
import { changeValue } from "../../../reducers/cardReducer";
import ProductCards from "./ProductCards";
import "./cards.css";
import { cartEvent } from "../../../reducers/cardReducer";
import {BsGithub} from "react-icons/bs";
import {MdOutlineAddShoppingCart} from "react-icons/md";
import data from "../../../data.json"

class Cards extends Component {

  hover=false
  mouseOver=()=>{
    this.hover=!this.hover
  }

  render() {
    let productsCount=data.products.length;
    if(this.props.state.length>0){
      productsCount=this.props.state.length
    }else{
      productsCount=data.products.length
    }
    let count = 0;
    Object.keys(this.props.array).map((key) => {
      count += this.props.array[key].count;
    });
    
    return (
      <div className="main-container">
        {!this.props.cartStatus && <p className="count">{count}</p>}

        <div className="icons-container">
          <a href={"https://gitlab.com/bhuvankarthi"} onMouseOverCapture={(e)=>this.mouseOver(e)}>
          {!this.hover && <BsGithub className="git-icon" />}
          
          {this.hover && <img src={"/home/bhuvankarthi/reactProjects/shopping_site/src/static/products/81333-github.gif"}/>}
          </a>
          
          <MdOutlineAddShoppingCart className={
            this.props.cartStatus ? "bag-icon-disappear" : "bag-icon"
          } onClick={() => this.props.cartEvent()}/>
          
          
        </div>
        <section className="main-section">
          <div className="button-container">
            <div>Sizes:</div>
            <div className="buttons">
              <button
                className="button"
                value="XS"
                onClick={(event) => this.props.changeValue(event)}
              >
                XS
              </button>
              <button
                className="button"
                value="S"
                onClick={(event) => this.props.changeValue(event)}
              >
                S
              </button>
              <button
                className="button"
                value="M"
                onClick={(event) => this.props.changeValue(event)}
              >
                M
              </button>
              <button
                className="button"
                value="ML"
                onClick={(event) => this.props.changeValue(event)}
              >
                ML
              </button>
              <button
                className="button"
                value="L"
                onClick={(event) => this.props.changeValue(event)}
              >
                L
              </button>
              <button
                className="button"
                value="XL"
                onClick={(event) => this.props.changeValue(event)}
              >
                XL
              </button>
              <button
                className="button"
                value="XXL"
                onClick={(event) => this.props.changeValue(event)}
              >
                XXL
              </button>
            </div>
          </div>

          <div className="products-container">
            <div className="products-found">{`${productsCount} product(s) found`}</div>
            <div className="product-images-container">
              
              <ProductCards />
            </div>
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    state: state.dataReducer.products,
    array: state.dataReducer.array,
    count: state.dataReducer.count,
    cartStatus: state.dataReducer.cartStatus,
    activeStates: state.dataReducer.activeStates,
  };
};
const mapDispatchToProps = () => {
  return {
    changeValue,
    cartEvent,
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(Cards);
